sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function(Controller, MessageBox) {
	"use strict";

	return Controller.extend("sap.sapx05.controller.View1", {
		onInit: function() {
			var data = {};
			data.firstName = "Goode";
			data.lastName = "Student";
			var model = new sap.ui.model.json.JSONModel(data);
			model.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			sap.ui.getCore().setModel(model);
			var selectionData = {
				"selection": "Please Select",
				"data": [{
					"text": "Alisha",
					"key": "Alisha"
				}, {
					"text": "Burt",
					"key": "Burt"
				}, {
					"text": "Candice",
					"key": "Candice"
				}, {
					"text": "Donald",
					"key": "Donald"
				}, {
					"text": "Erika",
					"key": "Erika"
				}, {
					"text": "Frank",
					"key": "Frank"
				}, {
					"text": "Goode",
					"key": "Goode"
				}]
			};

			var model12 = new sap.ui.model.json.JSONModel(selectionData);
			model12.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			this.byId("inpFirstName").setModel(model12, "selections");
			sap.ui.getCore().getModel().setData({
				firstname: this.byId("inpFirstName").getModel("selections").getProperty("/selection")
			}, true);

		},
		onBtnClick: function() {
			// var firstName = this.byId("inpFirstName").getValue();
			var firstName = sap.ui.getCore().byId("idView1--vHTML--inpFirstName").getValue();
			var lastName = this.byId("inpLastName").getValue();
			var message = "First Name: " + firstName + "\n" + "Last Name: " + lastName;
			sap.m.MessageBox.show(message, {
				icon: sap.m.MessageBox.Icon.SUCCESS,
				title: "Button Pressed",
				actions: sap.m.MessageBox.Action.CLOSE
			});
		}
	});
});