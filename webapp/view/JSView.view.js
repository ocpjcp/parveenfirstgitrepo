sap.ui.jsview("sap.sapx05.view.JSView", {

	/** Specifies the Controller belonging to this View. 
	 * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	 * @memberOf sap.sapx05.view.view.JSView
	 */
	getControllerName: function() {
		return "sap.sapx05.controller.JSView";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	 * Since the Controller is given to this method, its event handlers can be attached right away. 
	 * @memberOf sap.sapx05.view.view.JSView
	 */
	createContent: function(oController) {
		var oPage = new sap.m.Page({
			title: "Exercise 5- Javascript View",
			content: [
				new sap.m.Label({
					id: "lblFirstName",
					text: "First Name:"
				}),
				new sap.m.Input({
					id: "inpFirstName",
					value: ""
				}),
				new sap.m.Label({
					id: "lblLastName",
					text: "Last Name:"
				}),
				new sap.m.Input({
					id: "inpLastName",
					value: ""
				}),
				new sap.m.Button({
					id: "btnSubmit",
					text: "Submit"
				})
			]
		});

		var app = new sap.m.App("myApp", {
			initialPage: "oPage"
		});
		app.addPage(oPage);
		return app;
	}

});